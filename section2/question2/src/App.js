import React from "react";
import "./App.css";

const App = () => {
  const [items, setItems] = React.useState([]);
  const [search, setSearch] = React.useState("");

  React.useEffect(() => {
    fetch("https://api.publicapis.org/categories")
      .then((res) => res.json())
      .then((response) => {
        setItems(response);
      });
  }, []);

  const handleOnChange = (e) => {
    setSearch(e.target.value);
  };

  const matchSearch = (item) => {
    const regex = new RegExp(`${search}`, "i");
    return regex.test(item);
  };

  const renderContent = () => {
    return (
      <table style={{ margin: "auto" }}>
        <thead>
          <tr>
            <th>Catagories</th>
          </tr>
        </thead>
        <tbody>
          {items.filter(matchSearch).map((item, index) => (
            <tr key={index}>
              <td>{item}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <div style={{ margin: "20px", textAlign: "center" }}>
      <div>
        <input type="text" value={search} onChange={handleOnChange}></input>
        {renderContent()}
      </div>
    </div>
  );
};

export default App;
