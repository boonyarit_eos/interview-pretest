function thisIsSyncFunction() {
  let result = 0;

  fetch("https://codequiz.azurewebsites.net/data")
    .then((res) => res.json())
    .then((response) => {
      result = response.data;
    });
  return result;
}

const number1 = thisIsSyncFunction();
const calculation = number1 * 10;
console.log(calculation);
