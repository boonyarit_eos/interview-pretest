const puppeteer = require("puppeteer");

const myArgs = process.argv.slice(2);
const selector = myArgs[0];

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto("https://codequiz.azurewebsites.net/", {
    waitUntil: "networkidle2",
  });

  const hrefElement = await page.$x(
    "//p[contains(., 'You need to accept cookie to continue')]"
  );
  if (hrefElement) {
    await Promise.all([
      page.click("input[type=button]"),
      page.waitForNavigation(),
    ]);
  }

  const rows = await page.$$("table > tbody > tr");

  let headers = [];
  const data = {};

  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    if (i == 0) {
      headers = await row.$$eval("th", (nodes) =>
        nodes.map((n) => n.innerText)
      );
    } else {
      const body = await row.$$eval("td", (nodes) =>
        nodes.map((n) => n.innerText)
      );
      for (let j = 0; j < body.length; j++) {
        if (j == 0) {
          data[body[j]] = {};
        } else {
          data[body[0]][headers[j]] = body[j];
        }
      }
    }
  }

  if (data[selector] && data[selector]['Nav']) {
    console.log(data[selector]["Nav"]);
  } else {
    console.log("Data Not Found.");
  }

  await browser.close();
})();
