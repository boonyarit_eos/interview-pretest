import "./App.css";
import React from "react";

const App = () => {
  const [number, setNumber] = React.useState(1);
  const [select, setSelect] = React.useState("prime");

  const isPrime = (num) => {
    for (var i = 2; i < num; i++) if (num % i === 0) return false;
    return num > 1;
  }

  const isPerfectSquare = (x) => {
    let s = parseInt(Math.sqrt(x));
    return s * s === x;
  }

  const isFibonacci = (n) => {
    return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4);
  }

  const handleInputChange = (e) => {
    let value = e.target.value;

    if (value !== "") {
      value = parseFloat(value)
      value = value >= 0 ? value : 1
      value = Math.round(value)
    }

    setNumber(value);
  };

  const handleSelectChange = (e) => {
    const value = e.target.value;
    setSelect(value);
  };

  const displayResult = () => {
    if (number < 0) {
      return false
    }

    if (select === "prime") {
      return isPrime(number);
    } else {
      return isFibonacci(number);
    }
  };

  return (
    <div className="container">
      <div className="firstCol">
        <input type="number" value={number} onChange={handleInputChange} />
      </div>
      <div className="expandCol">
        <select onChange={handleSelectChange} value={select}>
          <option value="prime">isPrime</option>
          <option value="fibo">isFibonacci</option>
        </select>
      </div>
      <div className="lastCol">{displayResult() ? "True" : "False"}</div>
    </div>
  );
}

export default App;
