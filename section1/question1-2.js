const thisIsSyncFunction = async function () {
  try {
    const res = fetch("https://codequiz.azurewebsites.net/data");
    if (res) {
      return res.json().data;
    }
    return 0;
  } catch (err) {
    return 0;
  }
};

const calculate = async function () {
  const number1 = await thisIsSyncFunction();
  const calculation = number1 * 10;
  console.log(calculation);
};

calculate();
